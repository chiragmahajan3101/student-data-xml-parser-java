import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.io.*;

public class GetXmlData 
{
	public String[][] readData() {
		String[][] temp = new String[1][1];
		try{
			File file = new File("D:\\ADV Java\\XMLParser\\StudentData.xml");
			BufferedReader br = new BufferedReader(new FileReader(file));	
			String tempData = "";
			StringBuffer xmlData = new StringBuffer("");			
			while ((tempData = br.readLine()) != null) {				
				xmlData.append(tempData.trim());
			}   
			int i = 0;   				 				
			String totalNumberRegex = "(<Student>)(.*?)(</Student>)";
			Pattern totalNumberPattern = Pattern.compile(totalNumberRegex);
			Matcher totalNumberMatcher = totalNumberPattern.matcher(xmlData);
			while(totalNumberMatcher.find()) {
				i++;
			}						
			String groupRegex = "(<Student>)(<StudentName>)(.*?)(</StudentName>)(<StudentRoll>)(.*?)(</StudentRoll>)(<StudentEmail>)(.*?)(</StudentEmail>)(<StudentPhone>)(.*?)(</StudentPhone>)(</Student>)";
			String[][] dataStoreToDisplay = new String[i][4];						
			Pattern groupPattern = Pattern.compile(groupRegex);
			Matcher groupMatcher = groupPattern.matcher(xmlData);
			i=0;
			while(groupMatcher.find()) {				
				String name = groupMatcher.group(3);
				String roll = groupMatcher.group(6);
				String email = groupMatcher.group(9);
				String phone = groupMatcher.group(12);
				// System.out.println(name + " " + roll + " " + email + " " + phone);				
				dataStoreToDisplay[i][0] = name;
				dataStoreToDisplay[i][1] = roll;
				dataStoreToDisplay[i][2] = email;
				dataStoreToDisplay[i][3] = phone;
				i++;				
			}			
			return dataStoreToDisplay;			
		} catch(IOException e) {
			System.out.println(e.getMessage());
		}		
		return temp;
	}

}