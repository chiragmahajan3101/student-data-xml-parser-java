import java.io.*;
import java.nio.file.*;
import java.util.*;

public class WriteXmlData {
    public String convertDataToXml(String name, String roll, String email, String phone) {
        String xmlFormat = "<Student><StudentName>" + name + "</StudentName><StudentRoll>" + roll
                + "</StudentRoll><StudentEmail>" + email + "</StudentEmail><StudentPhone>" + phone
                + "</StudentPhone></Student>";
        return xmlFormat;
    }

    public String[][] write(String data) {
        String fileName = "StudentData.xml";
        GetXmlData obj = new GetXmlData();
        String[][] newData = obj.readData();
        try {
            List<String> list = Files.readAllLines(Paths.get("D:\\ADV Java\\XMLParser\\StudentData.xml"));
            // list.add(list.size() - 1, "<Student><StudentName>Vicky
            // Jain</StudentName><StudentRoll>1902099</StudentRoll><StudentEmail>vickyjain@gmail.com</StudentEmail><StudentPhone>+91
            // 9423972290</StudentPhone></Student>");
            list.add(list.size() - 1, data);
            Files.write(Paths.get("D:\\ADV Java\\XMLParser\\StudentData.xml"), list);
            newData = obj.readData();
            return newData;
        } catch (IOException e) {
            System.out.println(e.getMessage());
        }
        return newData;
    }
}