import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Validator {
    private String emailRegex = "^[A-Za-z_]([\\.A-Za-z0-9\\+-_]+)*@([A-Za-z_]([A-Za-z0-9-])*)(\\.[A-Za-z0-9-]+)*(\\.[A-Za-z0-9]{2,})$";
    private String mobileNumberRegex = "^\\+\\d{2}\\s[6-9]\\d{9}$";

    public boolean validateEmail(String email) {
        Pattern emailPattern = Pattern.compile(emailRegex);
        Matcher emailMatcher = emailPattern.matcher(email);
        return emailMatcher.matches();
    }

    public boolean validatePhone(String mobileNumber) {
        Pattern mobileNumberPattern = Pattern.compile(mobileNumberRegex);
        Matcher mobileNumberMatcher = mobileNumberPattern.matcher(mobileNumber);
        return mobileNumberMatcher.matches();
    }

}