import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

public class StudentDataTableFrame extends JFrame {
    static JTextField nameText, rollText, emailText, phoneText;
    static JButton submit;
    static String data[][];
    static JScrollPane sp;
    static JTable jt;
    static String column[] = { "NAME", "ROLL` NO", "EMAIL", "PHONE" };

    public StudentDataTableFrame(String title) {
        super(title);
        setSize(460, 400);
        addWindowListener(new WindowClosingAdapter());
    }

    public static void main(String[] args) {
        StudentDataTableFrame appFrame = new StudentDataTableFrame("Student Data");
        appFrame.setLayout(new BorderLayout(10, 10));
        JPanel formPanel = new JPanel();

        ImageIcon nameIcon = new ImageIcon("images/names.png");
        ImageIcon rollIcon = new ImageIcon("images/roll.png");
        ImageIcon emailIcon = new ImageIcon("images/email.png");
        ImageIcon phoneIcon = new ImageIcon("images/phone.png");
        JLabel nameLabel = new JLabel("Name");
        JLabel rollLabel = new JLabel("Roll No");
        JLabel emailLabel = new JLabel("Email");
        JLabel phoneLabel = new JLabel("Phone No");
        nameLabel.setIcon(nameIcon);
        rollLabel.setIcon(rollIcon);
        emailLabel.setIcon(emailIcon);
        phoneLabel.setIcon(phoneIcon);
        nameText = new JTextField(30);
        rollText = new JTextField(30);
        emailText = new JTextField(30);
        phoneText = new JTextField(30);

        submit = new JButton("SUBMIT");

        GetXmlData getDataObj = new GetXmlData();
        data = getDataObj.readData();
        jt = new JTable(data, column);
        // jt.setBounds(30,40,150,300);
        sp = new JScrollPane(jt);

        // submit.addActionListener(this);

        submit.addActionListener(new ActionOnSubmit(appFrame));

        formPanel.add(nameLabel);
        formPanel.add(nameText);
        formPanel.add(rollLabel);
        formPanel.add(rollText);
        formPanel.add(emailLabel);
        formPanel.add(emailText);
        formPanel.add(phoneLabel);
        formPanel.add(phoneText);
        formPanel.add(submit);
        formPanel.setPreferredSize(new Dimension(460, 200));
        appFrame.add(formPanel, BorderLayout.NORTH);
        formPanel.setBackground(Color.gray);
        appFrame.add(sp);
        appFrame.setVisible(true);
    }
}

class WindowClosingAdapter extends WindowAdapter {
    public void windowClosing(WindowEvent we) {
        System.exit(0);
    }
}

class ActionOnSubmit implements ActionListener {
    private StudentDataTableFrame frameRefObj;
    public JDialog dialogBox;
    public JTextArea textBox;

    public ActionOnSubmit(StudentDataTableFrame refObj) {
        frameRefObj = refObj;
    }

    public void actionPerformed(ActionEvent ae) {

        String name = StudentDataTableFrame.nameText.getText();
        String roll = StudentDataTableFrame.rollText.getText();
        String email = StudentDataTableFrame.emailText.getText();
        String phone = StudentDataTableFrame.phoneText.getText();

        if ((StudentDataTableFrame.nameText.getText().equals(""))
                || (StudentDataTableFrame.rollText.getText().equals(""))
                || (StudentDataTableFrame.emailText.getText().equals(""))
                || (StudentDataTableFrame.phoneText.getText().equals(""))) {
            System.out.println("Some Field Is Missing!");
            dialogBox = new JDialog(frameRefObj, "Missing Field", true);
            dialogBox.setLayout(new FlowLayout());
            JButton b = new JButton("OK");
            b.addActionListener(new ActionListener() {
                public void actionPerformed(ActionEvent e) {
                    dialogBox.setVisible(false);
                }
            });
            textBox = new JTextArea();
            textBox.setText("Some Fields are Missing!" + "\n" + "Fill all the fields to proceed");
            textBox.setEditable(false);
            dialogBox.add(textBox);
            dialogBox.add(b);
            dialogBox.setPreferredSize(new Dimension(350, 200));
            dialogBox.pack();
            dialogBox.setVisible(true);
            return;
        } else {
            System.out.println("No Blank");
            Validator validateObj = new Validator();
            if (validateObj.validateEmail(email) && validateObj.validatePhone(phone)) {
                updateTable(name, roll, email, phone);
            } else {
                System.out.println("Failed Validation");
                if (!validateObj.validateEmail(email)) {
                    if (!validateObj.validatePhone(phone)) {
                        dialogBox = new JDialog(frameRefObj, "Failed Validation", true);
                        dialogBox.setLayout(new FlowLayout());
                        JButton b = new JButton("OK");
                        b.addActionListener(new ActionListener() {
                            public void actionPerformed(ActionEvent e) {
                                dialogBox.setVisible(false);
                            }
                        });
                        textBox = new JTextArea();
                        textBox.setText(
                                "Both Mobile No and Email has failed Validation.\n For mobile please enter country code!");
                        textBox.setEditable(false);
                        dialogBox.add(textBox);
                        dialogBox.add(b);
                        dialogBox.setPreferredSize(new Dimension(350, 200));
                        dialogBox.pack();
                        dialogBox.setVisible(true);
                    } else {
                        dialogBox = new JDialog(frameRefObj, "Failed Email Validation", true);
                        dialogBox.setLayout(new FlowLayout());
                        JButton b = new JButton("OK");
                        b.addActionListener(new ActionListener() {
                            public void actionPerformed(ActionEvent e) {
                                dialogBox.setVisible(false);
                            }
                        });
                        textBox = new JTextArea();
                        textBox.setText("Enter Correct Email Address.");
                        textBox.setEditable(false);
                        dialogBox.add(textBox);
                        dialogBox.add(b);
                        dialogBox.setPreferredSize(new Dimension(350, 200));
                        dialogBox.pack();
                        dialogBox.setVisible(true);
                    }
                } else {
                    dialogBox = new JDialog(frameRefObj, "Failed Phone Validation", true);
                    dialogBox.setLayout(new FlowLayout());
                    JButton b = new JButton("OK");
                    b.addActionListener(new ActionListener() {
                        public void actionPerformed(ActionEvent e) {
                            dialogBox.setVisible(false);
                        }
                    });
                    textBox = new JTextArea();
                    textBox.setText(
                            "Enter Correct Mobile No.\n For mobile please enter country code. Eg: +91 9999999999");
                    textBox.setEditable(false);
                    dialogBox.add(textBox);
                    dialogBox.add(b);
                    dialogBox.setPreferredSize(new Dimension(350, 200));
                    dialogBox.pack();
                    dialogBox.setVisible(true);
                }
                return;
            }
        }
    }

    private void updateTable(String name, String roll, String email, String mobile) {
        StudentDataTableFrame.nameText.setText("");
        StudentDataTableFrame.rollText.setText("");
        StudentDataTableFrame.phoneText.setText("");
        StudentDataTableFrame.emailText.setText("");
        WriteXmlData writeDataObj = new WriteXmlData();
        StudentDataTableFrame.data = writeDataObj.write(writeDataObj.convertDataToXml(name, roll, email, mobile));
        StudentDataTableFrame.jt = new JTable(StudentDataTableFrame.data, StudentDataTableFrame.column);
        StudentDataTableFrame.sp.setViewportView(StudentDataTableFrame.jt);
    }

}