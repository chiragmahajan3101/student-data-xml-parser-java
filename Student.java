public class Student {
    private String name;
    private String roll;
    private String email;
    private String phone;

    public Student(String name, String roll, String email, String phone) {
        this.name = name;
        this.roll = roll;
        this.email = email;
        this.phone = phone;
    }
}